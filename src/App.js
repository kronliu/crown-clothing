import React, { useState, useEffect } from "react";
import "./App.css";
import Home from "./pages/Home/home.component";
import Shop from "./pages/Shop/shop.component";
import SignIn from "./pages/SignInAndSignUp/sign-in-and-sign-up.component";
import Header from "./components/Header/header.component";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { auth } from "./firebase/firebase.utils";

const App = () => {
  const [currentUser, setCurrentUser] = useState(null);
  useEffect(() => {
    auth.onAuthStateChanged((user) => {
      setCurrentUser(user);
      console.log(user);
    });
  }, []);

  return (
    <BrowserRouter>
      <Header currentUser={currentUser} />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/shop" component={Shop} />
        <Route exact path="/signin" component={SignIn} />
      </Switch>
    </BrowserRouter>
  );
};

export default App;
