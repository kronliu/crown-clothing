import React from "react";
import "./sign-in-and-sign-up.styles.scss";
import SignIn from "../../components/SignIn/sign-in.component";

const SignInAndSignUp = () => {
  return (
    <div className="sign-in-and-sign-up">
      <SignIn />
    </div>
  );
};

export default SignInAndSignUp;
