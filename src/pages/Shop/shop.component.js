import React, { useState } from "react";
import SHOP_DATA from "./shop.data";
import CollectionPreview from "../../components/CollectionPreview/collection-preview.component";

const Shop = () => {
  const [collections, setCollections] = useState(SHOP_DATA);
  return (
    <div>
      {collections.map(({ ...collectionProps }) => (
        <CollectionPreview
          key={{ ...collectionProps }.id}
          {...collectionProps}
        />
      ))}
    </div>
  );
};

export default Shop;
