import React from "react";
import "./home.styles.scss";

import Directory from "../../components/Directory/directory.component";

const Home = () => (
  <div className="home">
    <Directory />
  </div>
);

export default Home;
