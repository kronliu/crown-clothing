import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

const config = {
  apiKey: "AIzaSyAn1YvOxaXWNx8pRr4r2YsTY5z8T0-UIW8",
  authDomain: "crown-db-9ee6d.firebaseapp.com",
  databaseURL: "https://crown-db-9ee6d.firebaseio.com",
  projectId: "crown-db-9ee6d",
  storageBucket: "crown-db-9ee6d.appspot.com",
  messagingSenderId: "1068457342385",
  appId: "1:1068457342385:web:099e45eaa021d25cd27990",
  measurementId: "G-RLYHK3583P",
};

firebase.initializeApp(config);

export const auth = firebase.auth();
export const firestore = firebase.firestore();

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({ prompt: "select_account" });
export const signInWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;
