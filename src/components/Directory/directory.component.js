import React, { useState } from "react";
import "./directory.styles.scss";

import MenuItem from "../MenuItem/menu-item.component";
import DIRECTORY_SECTIONS from "./directory.sections";

const Directory = () => {
  const [sections, setSections] = useState(DIRECTORY_SECTIONS);
  return (
    <>
      <div className="directory-menu">
        {sections.map(({ ...sectionProps }) => (
          <MenuItem key={sectionProps.id} {...sectionProps} />
        ))}
      </div>
    </>
  );
};

export default Directory;
