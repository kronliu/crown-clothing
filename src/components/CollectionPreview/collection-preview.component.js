import React from "react";
import "./collection-preview.styles.scss";
import CollectionItem from "../CollectionItem/collection-item.component";

const CollectionPreview = ({ title, items }) => {
  return (
    <div className="collection-preview">
      <h1 className="title">{title}</h1>
      <div className="preview">
        {items
          .filter((item, index) => index < 4)
          .map(({ ...itemProps }) => (
            <CollectionItem key={itemProps.id} {...itemProps} />
          ))}
      </div>
    </div>
  );
};

export default CollectionPreview;
