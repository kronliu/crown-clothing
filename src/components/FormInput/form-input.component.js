import React from "react";
import SignIn from "../SignIn/sign-in.component";
import "./form-input.styles.scss";

const FormInput = ({ ...props }) => {
  return (
    <div className="group">
      <input
        className="form-input"
        onChange={props.onChange}
        value={props.value}
        type={props.type}
      />
      {props.label ? (
        <label
          className={`${props.value.length ? "shrink" : ""} form-input-label`}
        >
          {props.label}
        </label>
      ) : null}
    </div>
  );
};

export default FormInput;
